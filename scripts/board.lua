board = 
{
	{ 'w', ' ', 'w', ' ', 'w', ' ', 'w', ' ' },
	{ ' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w' },
	{ 'w', ' ', 'w', ' ', 'w', ' ', 'w', ' ' },
	{ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
	{ ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
	{ ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b' },
	{ 'b', ' ', 'b', ' ', 'b', ' ', 'b', ' ' },
	{ ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b' },
}

player, enemy = 'w', 'b'
whiteAi, blackAi = false, false
captured = {}
busy = false

function SetBoardCell(x, y)
	if board[x][y]:lower() == player and not move then
		sel = {x = x, y = y}
		if player == 'w'and whiteAi then White_Move()
		elseif player == 'b' and blackAi then Black_Move()
		end
	
	elseif board[x][y] == ' ' and sel ~= nil then
		local step = { x = x-sel.x, y = y-sel.y }

		if (board[sel.x][sel.y] == 'w' and step.x == 1 or board[sel.x][sel.y] == 'b' and step.x == -1) and 
			math.abs(step.y) == 1 and not move then
			
			if player == 'w' and x == 8 then board[x][y] = 'W'
			elseif player == 'b' and x == 1 then board[x][y] = 'B'
			else board[x][y] = board[sel.x][sel.y]
			end
			board[sel.x][sel.y] = ' '


		elseif board[sel.x][sel.y] == player and math.abs(step.x) == 2 and math.abs(step.y) == 2 then

			local leaped = { x = sel.x+step.x/2, y = sel.y+step.y/2 }
			if board[leaped.x][leaped.y]:lower() == enemy then
				captured[#captured+1] = { x = leaped.x, y = leaped.y }
				board[leaped.x][leaped.y] = 'x'

				if player == 'w' and x == 8 then board[x][y] = 'W'
				elseif player == 'b' and x == 1 then board[x][y] = 'B'
				else board[x][y] = board[sel.x][sel.y] 
				end
				
				board[sel.x][sel.y] = ' '

				if  (x < 7 and y < 7 and board[x+2][y+2] == ' ' and board[x+1][y+1]:lower() == enemy) or
					(x < 7 and y > 2 and board[x+2][y-2] == ' ' and board[x+1][y-1]:lower() == enemy) or
				    (x > 2 and y < 7 and board[x-2][y+2] == ' ' and board[x-1][y+1]:lower() == enemy) or
					(x > 2 and y > 2 and board[x-2][y-2] == ' ' and board[x-1][y-1]:lower() == enemy) then
					
					sel.x, sel.y, move = x, y, true
					return
				end
			else return
			end
		
		elseif board[sel.x][sel.y] == player:upper() and math.abs(step.x) == math.abs(step.y) then

			local d = { x = step.x / math.abs(step.x), y = step.y / math.abs(step.y) }
			local savechecker = nil
			for i = 1, math.abs(step.x) do
				local leaped = { x = sel.x+i*d.x, y = sel.y+i*d.y }
				if not savechecker and board[leaped.x][leaped.y] == enemy then
					captured[#captured+1] = { x = leaped.x, y = leaped.y }
					savechecker, board[leaped.x][leaped.y] = board[leaped.x][leaped.y], 'x'
				elseif savechecker and board[leaped.x][leaped.y] == enemy then
					board[captured[#captured].x][captured[#captured].y] = savechecker
					captured[#captured] = nil
					return
				elseif board[leaped.x][leaped.y] ~= ' ' and board[leaped.x][leaped.y] ~= 'x' then
					return
				end
			end

			if move and not savechecker then return end
			board[x][y], board[sel.x][sel.y] = board[sel.x][sel.y], ' '

			if savechecker then
				local i, j = x+1, y+1
				while i < 8 and j < 8 and board[i][j]:lower() ~= player do
					if board[i][j]:lower() == enemy and board[i+1][j+1] == ' ' then
						sel.x, sel.y, move = x, y, true
						return
					end
					i, j = i+1, j+1
				end
				i, j = x-1, y-1
				while i > 1 and j > 1 and board[i][j]:lower() ~= player do
					if board[i][j]:lower() == enemy and board[i-1][j-1] == ' ' then
						sel.x, sel.y, move = x, y, true 
						return 
					end
					i, j = i-1, j-1
				end
				i, j = x+1, y-1
				while i < 8 and j > 1 and board[i][j]:lower() ~= player do
					if board[i][j]:lower() == enemy and board[i+1][j-1] == ' ' then
						sel.x, sel.y, move = x, y, true
						return 
					end
					i, j = i+1, j-1
				end
				i, j = x-1, y+1
				while i > 1 and j < 8 and board[i][j]:lower() ~= player do
					if board[i][j]:lower() == enemy and board[i-1][j+1] == ' ' then
						sel.x, sel.y, move = x, y, true
						return 
					end
					i, j = i-1, j+1
				end
			end

		else return
		end

		for i = 1, #captured do board[captured[i].x][captured[i].y] = ' ' end
		captured = {}

		sel, move = nil, false
		player, enemy = enemy, player
		busy = false

		print(whiteAi, blackAi)

		if player == 'w' and whiteAi then 
			busy = true
			White_Select()
		elseif player == 'b' and blackAi then 
			busy = true;
			Black_Select()
		end
	end
end