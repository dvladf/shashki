dofile("scripts/white_func.lua")

-- row , col
-- board - is a board
--
heuristic = {}
heuristic.check = {}
heuristic.move = {}

function White_Select(first)
    if first == nil then
        first = true
    end

    local move = white_func.check_heuristic(white_func.count_strategy('w'))
    --[[heuristic.check = {}
    heuristic.move = {}
    table.insert(heuristic.check, move[1])
    table.insert(heuristic.move, move[2])
    if first then
        SetBoardCell(heuristic.check[1][1], heuristic.check[1][2])
    else
        --print_r(heuristic)
        White_Move()
    end
    ]]

end

function White_Move()
    --[[io.write(" * ")
    SetBoardCell(heuristic.move[1][1], heuristic.move[1][2])
    ]]
end

function print_table(table) 
    io.write('*********************************\n')
    for i = 1,#table do
        io.write(table[i][1], ' * ', table[i][2])
        io.write('\n');
    end
end
