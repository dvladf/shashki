white_func = {}
white_func.turns = {}

white_func.board = {}


white_func.find_moves_check = function (row, col, cur_move, color)
    local friend = {'b', 'B'}
    local enemy = {'w', 'W'}
    if color == 'w' then
        enemy = {'b', 'B'}
        friend = {'w', 'W'}
    end
    local move = {{0,0}, {0,0}, -1, {}} -- {{current pos}, {pos to go}, best_point}
    local cells = {
        { 1,  1},
        { 1, -1},
        {-1,  1},
        {-1, -1}
    }
    for i = 1, #cells do
        if cur_move ~= nil then
            --print(row, col)
            if cur_move[1] == -cells[i][1] and cur_move[2] == -cells[i][2]  then
                i=i+1
                if i > 4 then
                    return move
                end
            end
        end
        local r = row + cells[i][1]
        local c = col + cells[i][2]
        if 0 < r and r < 9 and 0 < c and c < 9 and arny.in_tbl(board[r][c], friend) == 0 then
            if arny.in_tbl(board[r][c], enemy) ~= 0 then
                local r = r + cells[i][1]     -- row_next
                local c = c + cells[i][2]     -- col_next
                if 0<r and r<9 and 0<c and c<9 and board[r][c]==' ' then
                    local m = white_func.find_moves_check(r, c, cells[i], color)
                    if move[3] < m[3]+2 then
                        move = {{row, col}, {r, c}, m[3] + 2, m}
                    end
                end
            else
                if move[3] < 0 and cells[i][1] ~= -1 then
                    move = {{row, col}, {r, c}, 0, {}}
                end
            end
        end
    end
    return move
end

white_func.find_moves_king = function (row, col, cur_move, color)
    local friend = {'b', 'B'}
    local enemy = {'w', 'W'}
    if color == 'w' then
        enemy = {'b', 'B'}
        friend = {'w', 'W'}
    end
    local move = {{0,0}, {0,0}, -1, {}} -- {{current pos}, {pos to go}, best_point}, {next_move}
    local cells = {
        { 1,  1},
        { 1, -1},
        {-1,  1},
        {-1, -1}
    }
    for i = 1, #cells do
        for j = 1, 8 do
            if cur_move ~= nil then
                if cur_move[1] == -cells[i][1]*j and cur_move[2] == -cells[i][2]*j  then
                    i=i+1
                    if i > 4 then
                        return move
                    end
                end
            end
            local r = row + cells[i][1]*j
            local c = col + cells[i][2]*j

            if 0 < r and r < 9 and 0 < c and c < 9 and arny.in_tbl(board[r][c], friend) == 0 then
                if arny.in_tbl(board[r][c], enemy) ~= 0 then
                    local r = r + cells[i][1]     -- row_next
                    local c = c + cells[i][2]     -- col_next
                    if 0<r and r<9 and 0<c and c<9 and board[r][c]==' ' then
                        local m = white_func.find_moves_king(r, c, cells[i], color)
                        if move[3] < m[3]+2 then
                            move = {{row, col}, {r, c}, m[3] + 2, m}
                        end
                    end
                else
                    if move[3] < 0 and cells[i][1] ~= -1 then
                        move = {{row, col}, {r, c}, 0, {}}
                    end
                end
            end
        end
    end
    return move
end
white_func.count_strategy = function (color)
    local king = 'B'
    if color == 'w' then
        king = 'W'
    end
    local moves = {}
    for row = 1, #board do
        for col = 1, #board do
           if board[row][col] == color then
               local move = white_func.find_moves_check(row, col, nil, color)
               if next(move) ~=nil then
                   table.insert(moves, move)
               end
           else
               if board[row][col] == king then
                   local move = white_func.find_moves_king(row, col, nil, color)
                   if next(move) ~=nil then
                       table.insert(moves, move)
                   end
               end
           end
        end
    end
    return moves
end


white_func.check_heuristic = function (moves)
    local max = -1
    local move = {}
    for i = 1, #moves do
        if moves[i][3] > max then
            move = moves[i]
            max = moves[i][3]
        end
    end
    --[[
    io.write("(---)\r\n)")
    print_r(move)
    io.write("(---)\r\n)")
    ]]
    --print_r(move)
    white_func.im_count(move)
    white_func.move(move)
    return move
end

white_func.move = function(move) 
    if #move ~= 0  and move[3] ~= -1 then
        SetBoardCell(move[1][1], move[1][2])
        SetBoardCell(move[2][1], move[2][2])
        white_func.move(move[4])
    end

end

white_func.im_count = function(move)
    local b = deepcopy(board)
    white_func.board = white_func.im_move(move, b)
    --print_board(white_func.board)
    
end
white_func.im_move = function(move, board)
    if #move ~= 0 and move[3] ~= -1 then
        local cur_row = move[1][1]
        local cur_col = move[1][2]
        local next_row = move[2][1]
        local next_col = move[2][2]
        --io.write(cur_row, ", ", cur_col, " -> ", next_row, ", ", next_col)
        board[next_row][next_col] = board[cur_row][cur_col]
        board[cur_row][cur_col] = ' '
        middle_row = (cur_row + next_row) / 2
        middle_col = (cur_col + next_col) / 2
        if math.floor(middle_row) == middle_row then
            board[middle_row][middle_col] = ' '
        end
        return white_func.im_move(move[4], board)
    end
    return board

end
arny = {}
function arny.in_tbl(value, table) 
    for i,v in ipairs(table) do
        if v == value then
            return i+1
        end
    end
    return 0
end

function print_r ( t )  
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_print_r(t,"  ")
        print("}")
    else
        sub_print_r(t,"  ")
    end
    print()
end

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end
function print_board(board)
    for row = 1,#board do
        io.write('*********************************\n* ')
        for col=1, #board do
            io.write(board[row][col], ' * ')
        end
        io.write('\n')
    end
    io.write('*********************************\n* ')
end
