#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <selene.h>

SDL_Renderer* renderer = nullptr;
SDL_Window* window = nullptr;

SDL_Texture* whiteTex = nullptr;
SDL_Texture* blackTex = nullptr;
SDL_Texture* whiteKingTex = nullptr;
SDL_Texture* blackKingTex = nullptr;
SDL_Texture* captureTex = nullptr;

SDL_Rect area{};
SDL_Rect rects[8 * 4];
SDL_Rect* pRects[8][8];

sel::State state{ true };

SDL_Texture* LoadTexture(const char* filename)
{
	SDL_Surface* surface = IMG_Load(filename);
	if (!surface) {
		std::cerr << "Created surface failed: " << SDL_GetError() << "\n";
		return nullptr;
	}
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	return texture;
}

void Resize()
{
	SDL_RenderGetViewport(renderer, &area);
	if (area.w >= area.h) {
		area.x = (area.w - area.h) / 2;
		area.w = area.h;
	} else {
		area.y = (area.h - area.w) / 2;
		area.h = area.w;
	}

	SDL_Rect* rect = rects;
	for (int i = 0; i < 8; ++i)
		for (int j = i % 2; j < 8; j += 2) {
			rect->w = rect->h = area.w / 8;
			rect->x = area.x + j * rect->w;
			rect->y = (area.y + area.h) - (i + 1) * rect->h;
			pRects[i][j] = rect++;
		}
}

bool Init(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		std::cerr << "SDL_Init failed: " << SDL_GetError() << "\n";
		return false;
	}
	IMG_Init(IMG_INIT_PNG);

	window = SDL_CreateWindow("Shashki", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, 800, 600,
		SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

	if (!window) {
		std::cerr << "Created window failed: " << SDL_GetError() << "\n";
		return false;
	}
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (!(whiteTex = LoadTexture("data/white.png"))) return false;
	if (!(blackTex = LoadTexture("data/black.png"))) return false;
	if (!(whiteKingTex = LoadTexture("data/wking.png"))) return false;
	if (!(blackKingTex = LoadTexture("data/bking.png"))) return false;
	if (!(captureTex = LoadTexture("data/checked.png"))) return false;
	if (!state.Load("scripts/board.lua")) return false;
	
	for (int i = 1; i < argc - 1; ++i) {
		std::cout << argv[1] << '\n';
		if (std::string(argv[i]) == "white") {
			if (!state.Load(argv[i+1])) return false;
			state["whiteAi"] = true;
		} 
		if (std::string(argv[i]) == "black") {
			if (!state.Load(argv[i+1])) return false;
			state["blackAi"] = true;
		} else 
			return false;
	}
	Resize();

	if (state["whiteAi"] == true && state["player"] == "w") 
		state["White_Select"]();

	return true;
}

void DrawBoard()
{
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xCE, 0x9E, 0xFF);
	SDL_RenderFillRect(renderer, &area);

	SDL_SetRenderDrawColor(renderer, 0xD1, 0x8B, 0x47, 0xFF);
	for (auto& rect : rects) SDL_RenderFillRect(renderer, &rect);

	for (int i = 0; i < 8; ++i)
		for (int j = i % 2; j < 8; j += 2) {
			auto cell = state["board"][i+1][j+1];
			if (cell == "w")
				SDL_RenderCopy(renderer, whiteTex, nullptr, pRects[i][j]);
			else if (cell == "b")
				SDL_RenderCopy(renderer, blackTex, nullptr, pRects[i][j]);
			else if (cell == "x")
				SDL_RenderCopy(renderer, captureTex, nullptr, pRects[i][j]);
			else if (cell == "W")
				SDL_RenderCopy(renderer, whiteKingTex, nullptr, pRects[i][j]);
			else if (cell == "B")
				SDL_RenderCopy(renderer, blackKingTex, nullptr, pRects[i][j]);
		}
}

void Render()
{
	SDL_RenderClear(renderer);
	DrawBoard();
	SDL_SetRenderDrawColor(renderer, 0xC1, 0xD9, 0xFF, 0xFF);
	SDL_RenderPresent(renderer);
}

bool Loop()
{
	bool isExit = false;
	SDL_Event ev;
	while (SDL_PollEvent(&ev)) {
		if (ev.type == SDL_QUIT) isExit = true;
		else if (ev.type == SDL_WINDOWEVENT) {
			if (ev.window.event == SDL_WINDOWEVENT_RESIZED) Resize();
		} else if (ev.type == SDL_MOUSEBUTTONDOWN && state["busy"] == false) {
			int xpos, ypos;
			SDL_GetMouseState(&xpos, &ypos);
			for (int i = 0; i < 8; ++i)
				for (int j = i % 2; j < 8; j += 2) {
					if (xpos > pRects[i][j]->x &&
						xpos < pRects[i][j]->x + pRects[i][j]->w &&
						ypos > pRects[i][j]->y &&
						ypos < pRects[i][j]->y + pRects[i][j]->h)
					{
						state["SetBoardCell"](i+1, j+1);
						goto endEvent;
					}
				}
		}
	endEvent:;
	}
	Render();
	return !isExit;
}

int Exit(int code)
{
	if (renderer) SDL_DestroyRenderer(renderer);
	if (window) SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();
	std::cout << "Press Enter for exit";
	getchar();
	return code;
}

int main(int argc, char* argv[])
{
	if (!Init(argc, argv)) return Exit(1);
	while (Loop());
	return Exit(0);
}